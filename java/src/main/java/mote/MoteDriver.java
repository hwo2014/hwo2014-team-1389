package mote;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import mote.message.SendMsg;

public class MoteDriver {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        try (Socket socket = new Socket(host, port)){
                PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
                BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

                new MoteBot(reader, writer, new SendMsg.Join(botName, botKey));
        } catch (Exception e) {
                System.out.println("Shit, something blew up.");
        }
    }
}
