package mote;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import mote.message.MsgWrapper;
import mote.message.SendMsg;

public class MoteBot {
    static final Gson gson = new Gson();
    private PrintWriter writer;

    public MoteBot(final BufferedReader reader, final PrintWriter writer, final SendMsg.Join join) throws JsonSyntaxException, IOException {
        this.writer = writer;
        String line = null;

        send(join);

        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            switch(msgFromServer.msgType) {
            case "carPositions":
                send(new SendMsg.Throttle(0.5));
                break;
            case "join":
                System.out.println("Joined as MoteBot!");
                break;
            case "gameInit":
                System.out.println("Race init as MoteBot!");
                break;
            case "gameEnd":
                System.out.println("Race end as MoteBot!");
                break;
            case "gameStart":
                System.out.println("Race start as MoteBot!");
                break;
        	default:
                send(new SendMsg.Ping());
            }
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}
