package mote.message;

import com.google.gson.Gson;

public abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();

    public static class Join extends SendMsg {
        public final String name;
        public final String key;

        public Join(final String name, final String key) {
            this.name = name;
            this.key = key;
        }

        @Override
        protected String msgType() {
            return "join";
        }
    }

    public static class Ping extends SendMsg {
        @Override
        protected String msgType() {
            return "ping";
        }
    }

    public static class Throttle extends SendMsg {
        private double value;

        public Throttle(double value) {
            this.value = value;
        }

        @Override
        protected Object msgData() {
            return value;
        }

        @Override
        protected String msgType() {
            return "throttle";
        }
    }

    public static class CreateRace extends SendMsg {
        public final String trackName;
        public final String password;
        public final int carCount;
        public final BotId botId;

        public CreateRace(String trackName, String password, int carCount, String botName, String botKey) {
            this.trackName = trackName;
            this.password = password;
            this.carCount = carCount;
            this.botId = new BotId(botName, botKey);
        }

        @Override
        protected String msgType() {
            return "createRace";
        }
    }

    public static class JoinRace extends SendMsg {
        public final String trackName;
        public final String password;
        public final int carCount;
        public final BotId botId;

        public JoinRace(String trackName, String password, int carCount, String botName, String botKey) {
            this.trackName = trackName;
            this.password = password;
            this.carCount = carCount;
            this.botId = new BotId(botName, botKey);
        }

        @Override
        protected String msgType() {
            return "joinRace";
        }
    }
}

class BotId {
    public final String name;
    public final String key;

    BotId(String name, String key) {
        this.name = name;
        this.key = key;
    }
}